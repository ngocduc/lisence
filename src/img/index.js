import test_online from './images.jpeg'
import bb from './bb.jpeg'
import ran from './ran.jpeg'
import wrong from './wrong.jpeg'

export {
    test_online,
    bb,
    ran,  
    wrong,
}