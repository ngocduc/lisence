import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveItem = (key, value) => {
	const convertValue = typeof value == 'object' ? JSON.stringify(value) : String(value)
	AsyncStorage.setItem(key, convertValue);
};

export const getItem = async (key) => {
	const result = await AsyncStorage.getItem(key);
	try {
		const dataParse = JSON.parse(result)
		return dataParse;
	} catch (e) {
		return result;
	}
}

