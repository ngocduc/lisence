export const KEY_STORE = {
    TEST_HIS: "TEST_HIS",
    USER_HIS: "USER_HIS",
    USER_WRONG: "USER_WRONG",
}

export const NUMBER = {
    QUESTION: 30,
    QUALIFIED: 22,
}