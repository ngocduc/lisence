
import ex1 from './file01.json';
import ex2 from './file02.json';
import ex3 from './file03.json';
import ex4 from './file04.json';
import ex5 from './file05.json';
import ex6 from './file06.json';
import ex7 from './file07.json';
import ex8 from './file08.json';
import ex9 from './file09.json';

export {
    ex1,
    ex2,
    ex3,
    ex4,
    ex5,
    ex6,
    ex7,
    ex8,
    ex9,
}