
import b1 from './b1.json';
import b2 from './b2.json';
import b3 from './b3.json';
import b4 from './b4.json';
import b5 from './b5.json';
import b6 from './b6.json';
import b7 from './b7.json';
import b8 from './b8.json';
import b9 from './b9.json';

import b10 from './b10.json';
import b11 from './b11.json';
import b12 from './b12.json';
import b13 from './b13.json';
import b14 from './b14.json';
import b15 from './b15.json';
import b16 from './b16.json';
import b17 from './b17.json';
import b18 from './b18.json';

export {
    b1,
    b2,
    b3,
    b4,
    b5,
    b6,
    b7,
    b8,
    b9,
    b10,
    b11,
    b12,
    b13,
    b14,
    b15,
    b16,
    b17,
    b18,
}