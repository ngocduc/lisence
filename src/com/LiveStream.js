import React from 'react';
import { TouchableOpacity, View, Text, Dimensions, StyleSheet, ImageBackground, Image, Linking } from 'react-native';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window');
import {
    test_online,
    bb,
    ran,
    wrong,
} from '../img'

function wp(percentage) {
    const value = (percentage * width) / 100;
    return Math.round(value);
}

const slideHeight = height * 0.36;
const slideWidth = wp(82);
const itemHorizontalMargin = wp(2);

export const sliderWidth = width;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const LiveStream = ({ navigation }) => {
    return (
        <View style={{ paddingVertical: 10, height: (width * 4 / 5) * 3 / 4 }}>
            <Carousel
                // ref={refCar}
                data={listTest}
                renderItem={({ item }) => <RenderItem item={item} navigation={navigation} />}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                parallaxFactor={0.4}
                // layout="stack" 
                layoutCardOffset={18}
                loop={true}
                loopClonesPerSide={2}
                autoplay={true}
                autoplayDelay={500}
                autoplayInterval={3000}
                keyExtractor={(item, index) => index + ''}
            // onSnapToItem={index => setIndex(index)}
            />
        </View>
    )
}

const RenderItem = ({ item, navigation }) => {
    return (
        <ImageBackground source={item.img} imageStyle={{ borderRadius: 10 }} style={[styles.itemLive, styles.shadowStyle]}>
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={item.colors}
                style={{
                    flex: 1,
                    borderRadius: 10,
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate(item.navigate)
                    }}
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderRadius: 5,
                        position: 'relative'
                    }}>
                    <View style={{ flex: 4, padding: 20 }}>
                        <Text numberOfLines={1} style={{ fontSize: 27, fontWeight: 'bold', color: '#fff' }}>{item.title}</Text>
                        <Text numberOfLines={3} style={{ fontSize: 20, marginTop: 10, fontWeight: 'bold', color: '#fff' }}>{item.sub}</Text>
                    </View>
                    <TouchableOpacity style={{ position: 'absolute', top: 10, right: 10 }}>
                        <IconEntypo name="dots-three-horizontal" style={{ color: '#fff', fontSize: 19 }} />
                    </TouchableOpacity>
                    {/* <View style={{ flex: 1 }}>

                </View> */}

                </TouchableOpacity>
            </LinearGradient>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width * 4 / 5,
        // height: 130,
        marginRight: 10,
        borderWidth: 1, borderColor: '#DFE5EA', borderRadius: 8,
        borderRadius: 10,
    },
    itemLive: {
        // height: 200, 
        flex: 1,
        borderRadius: 5,
        marginVertical: 10,
        resizeMode: "cover",
        justifyContent: "center",
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 10,
        // overflow: 'hidden'
    },
    shadowStyle: { shadowColor: 'black', shadowOffset: { width: -2, height: 2 }, shadowOpacity: 0.6, elevation: 2 },
})


export default LiveStream;

const listTest = [
    {
        colors: ['rgba(126, 189, 233, 0.8)', 'rgba(128, 163, 241, 0.9)'],
        img: test_online,
        title: "Bộ đề sát hạch",
        sub: "30 đề thi sát hạch cập nhật liên tục",
        navigate: 'ListExam',
    },
    {
        colors: ['rgba(137, 183, 113, 0.8)', 'rgba(177, 216, 118, 0.9)'],
        img: ran,
        title: "Luyện tập",
        sub: "Tạo đề thi ngẫu nhiên và luyện tập",
        navigate: 'Exam',
    },
    {
        colors: ['rgba(231, 169, 103, 0.8)', 'rgba(238, 160, 99, 0.9)'],
        img: bb,
        title: "Các biển báo",
        sub: "Tra cứu và học các biển báo đường bộ",
        navigate: 'Exam',
    },
    {
        colors: ['rgba(222, 102, 143, 0.8)', 'rgba(227, 91, 116, 0.9)'],
        img: wrong,
        title: "Top câu sai",
        sub: "Xem danh sách các câu sai và lời giải",
        navigate: 'Exam',
    },
]



const data = {
    "title": "test", "description": null, "banner": null,
    "questions": [
        {
            "id": "wbRBS", "title": "asdfasf", "images": [],
            "listAnswer": [
                { "id": "T6NGb", "content": "asdfasdf", "correct": false },
                { "id": "aGh65", "content": "asdasdf", "correct": true }
            ],
            "answer": "asdvasdvasdv"
        }
    ]
}
