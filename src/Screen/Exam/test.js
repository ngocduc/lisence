import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
    View, SafeAreaView, FlatList, TouchableOpacity,
    Dimensions, Text, ScrollView, StyleSheet, Alert, BackHandler
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { isEmpty, get } from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import { Modal, Dialog, Paragraph, Button } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';

import Header from './com/header';
import { RenderItem } from './com/test_item';
import CountDownHeader from './com/count_down_header'
import Result from './com/result';
import * as jsonData from '../../data/b2/data'
import { getItem, saveItem } from '../../hepper/handleStore';
import { KEY_STORE } from '../../hepper/const';

const { width, height } = Dimensions.get('window');

const Exam = ({ route, ...props }) => {
    const { test_id = 1 } = route.params || {};
    const [index, setIndex] = useState(0);
    const [animation, setAnimation] = useState('fadeIn');
    const [listExam, setListExam] = useState([])
    const [userAnswer, setUserSelect] = useState({})
    const [result, setResult] = useState(false)
    const [showConfirm, setShowConfirm] = useState(false)
    const [showResult, setShowResult] = useState(false)

    const [endGame, setEndGame] = useState(false)
    const [count, setCount] = useState(0)
    useFocusEffect(
        React.useCallback(() => {
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [test_id])
    );

    useEffect(() => {
        try {

            if (test_id) {
                const ex1 = jsonData[`b${test_id}`]
                if (ex1.questions) {
                    const dataInput = ex1.questions.map((item, index) => {
                        item.index = index;
                        return item
                    })
                    setListExam(dataInput)
                }
                // have test
                getItem(KEY_STORE.TEST_HIS)
                    .then((data) => {
                        if (data && !isEmpty(data[test_id])) {
                            setShowResult(true);
                            setEndGame(true);
                            setResult(data[test_id]);
                        }
                    })
                // have test
                getItem(KEY_STORE.USER_HIS)
                    .then((data) => {
                        if (data && !isEmpty(data[test_id])) {
                            setUserSelect(data[test_id]);
                        }
                    })


            } else {

            }
        } catch (err) {

        }
    }, [test_id])

    useEffect(() => {
        if (index) {
            setAnimation('fadeOut')
            setTimeout(() => {
                setAnimation('fadeIn')
            }, 100)
        }
    }, [index])
    // handle submit 
    const _handleEnd = ({ force = false, count = null } = {}) => {
        console.log('dnd', showResult)
        if (endGame) {
            setShowResult(true)
            return 1;
        }
        if (force) {
            setEndGame(true)
        }
        if (count) {
            setCount(count)
        }
        const objCheck = {};
        Object.keys(userAnswer).map(key => {
            const dataAnswer = userAnswer[key];
            const check = get(listExam, `[${key}].listAnswer`);
            let isCorrect = true;

            check.map((i, indexCheck) => {
                if (i.correct) {
                    if (!dataAnswer.includes(indexCheck)) {
                        isCorrect = false;
                    }
                } else {
                    if (dataAnswer.includes(indexCheck)) {
                        isCorrect = false;
                    }
                }
            })
            objCheck[key] = isCorrect;
        })
        setResult(objCheck);

        setShowConfirm({ force });
    }

    // handle submit and save 
    const _handleSubmit = async () => {
        setShowResult(true);
        setShowConfirm(false);
        setEndGame(true);
        const history = await getItem(KEY_STORE.TEST_HIS) || {}
        const use_history = await getItem(KEY_STORE.USER_HIS) || {}
        const use_wrong = await getItem(KEY_STORE.USER_WRONG) || []
        // console.log(history, 'history123')

        saveItem(KEY_STORE.TEST_HIS, {
            ...history,
            [test_id]: result,
        })
        saveItem(KEY_STORE.USER_HIS, {
            ...use_history,
            [test_id]: userAnswer,
        });
        // handle save wrong
        Object.keys(result).map(keyIndex => {
            use_wrong.unshift(listExam[keyIndex])
        })
        use_wrong.length = Math.min(use_wrong.length, 20)
        console.log(use_wrong.length, 'use_wrong')

        saveItem(KEY_STORE.USER_WRONG, use_wrong);


    }
    // handle clear 
    const _handleReset = async () => {
        setIndex(0);
        setUserSelect({})
        setResult(false)
        setShowConfirm(false)
        setShowResult(false)
        setEndGame(false)

        const history = await getItem(KEY_STORE.TEST_HIS) || {}
        const use_history = await getItem(KEY_STORE.USER_HIS) || {}

        if (history) {
            const { [test_id]: itemDel, ...rest } = history;
            console.log(1, 2, { rest, history })
            saveItem(KEY_STORE.TEST_HIS, rest)
        }
        if (use_history) {
            const { [test_id]: itemDel, ...rest } = history;
            console.log(33, { rest, history })
            saveItem(KEY_STORE.TEST_HIS, rest)
        }
    }

    const onBackPress = () => {
        console.log('-123123123123')
        Alert.alert("Kết thúc bài thi?", "Kết thúc bài thi và không chấm điểm", [
            {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
            },
            { text: "OK", onPress: () => props.navigation.goBack() }
        ])
        return true;
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <CountDownHeader _handleEnd={_handleEnd} endGame={endGame} />
            <Header index={+index + 1} setIndex={setIndex} total={listExam.length} userAnswer={userAnswer} />
            <Animatable.View animation={animation} style={{ flex: 1, }}>
                {listExam[index] && animation == 'fadeIn' && listExam[index] ?
                    <RenderItem
                        item={listExam[index]} index={index}
                        userAnswer={userAnswer[index]}
                        isShow={endGame}
                        onSubmit={val => setUserSelect({
                            ...userAnswer,
                            ...val,
                        })} /> : null}
            </Animatable.View>
            <NavigateIndex
                index={index}
                handleChangeIndex={setIndex}
                max={listExam.length}
                _handleEnd={_handleEnd}
            />
            {/* <Modal */}
            <Dialog visible={!!showConfirm} onDismiss={() => {
                if (get(showConfirm, 'force') && endGame) {
                } else {
                    setShowConfirm(false)
                }
            }}>
                <Dialog.Content>
                    <Paragraph style={{ fontSize: 20 }}>Kết thúc bài thi</Paragraph>
                    <Paragraph style={{ fontSize: 15 }}>Bạn muốn kết thúc bài thi và chấm điểm?</Paragraph>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button color='red' onPress={() => {
                        setShowConfirm(false)
                        if (endGame) {
                            props.navigation.goBack()
                        }
                    }}>Bỏ qua</Button>
                    <Button onPress={_handleSubmit}>Xem kết quả</Button>
                </Dialog.Actions>
            </Dialog>

            <Modal
                visible={showResult}
                onDismiss={() => { }}
                contentContainerStyle={{ backgroundColor: '#fff', width: width, height: height - 55, borderRadius: 10 }}
                style={{ marginTop: 55, borderRadius: 10 }}
            >
                <Result
                    result={result} navigation={props.navigation}
                    listExam={listExam} count={count}
                    _handlePressItem={(index) => {
                        setShowResult(false); setShowConfirm(false)
                        setIndex(index)
                    }}
                    _handleReset={_handleReset}
                />
            </Modal>
        </SafeAreaView>
    )

}


const fakeData =
{
    id: '123',
    question: 'Khái niệm "đường bộ" được hiểu thê snaof cho đúng? Đường bộ bao gồm:',
    img: ['link',],
    listAnswer: [
        {
            id: '2o34',
            content: 'Đường cầu đường bộ',
            correct: true,
        },
        {
            id: '2w3o4',
            content: 'Hầm đường bộ, phà đường bộ',
            correct: true,
        },
        {
            correct: false,
            id: '2o3w4',
            content: 'Đường cầu đường bộ hầm đường bộ, bến phà đường bộ và các công trình đường bộ phụ trợ khác'
        },
    ],
    answer: "Đường bộ thì chỉ liên quan đến đường không bao gồm công trình phụ trợ"
}


export default Exam;

const ListAn = ["A", "B", "C", "D", "E"]



export const NavigateIndex = ({ handleChangeIndex, index, max, openMod, _handleEnd }) => {
    return (
        <View style={stNavigate.container}>
            {index != 0 ?
                <TouchableOpacity
                    onPress={() => handleChangeIndex(+index - 1)}
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon type='MaterialCommunityIcons' name="chevron-left" style={{ color: '#A87A29', fontWeight: 'bold' }} />
                    <Text style={[stNavigate.text, { color: '#A87A29', fontWeight: 'bold' }]}> Trước </Text>
                </TouchableOpacity>
                : <View />}

            {+index != +max - 1 ?
                <TouchableOpacity
                    onPress={() => handleChangeIndex(+index + 1)}
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Text style={[stNavigate.text, { color: '#A87A29', fontWeight: 'bold' }]}>Tiếp theo </Text>
                    <Icon type='MaterialCommunityIcons' name="chevron-right" style={{ color: '#A87A29', fontWeight: 'bold' }} />
                </TouchableOpacity> :
                _handleEnd ?
                    <TouchableOpacity
                        onPress={() => _handleEnd()}
                        style={{
                            justifyContent: 'center',
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                    >
                        <Text style={[stNavigate.text, { color: '#A87A29', fontWeight: 'bold' }]}>Kết thúc </Text>
                        {/* <Icon type='MaterialCommunityIcons' name="chevron-right" style={{ color: '#A87A29', fontWeight: 'bold' }} /> */}
                    </TouchableOpacity>
                    : <View />
            }

        </View>
    )
}


const stNavigate = StyleSheet.create({
    container: {
        width: '100%',
        // backgroundColor: '#dedede',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        // borderTopColor: '#dedede',
        // borderTopWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    text: {
        fontSize: 18,
    },
    leftBtn: {
        // opacity: index == 1? 0.5: 1,
        // width: "40%",
        justifyContent: "center",
        alignItems: "center",

        // backgroundColor: 'red',
        // borderRadius: 25,
        // borderWidth: 4,
        paddingVertical: 10,
        // borderColor: '#e88812'
    },
    centerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "60%",
        height: '100%'
    },
    // rightBtn: {
    // 	width: "40%",
    // },
    next: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        paddingVertical: 12
    },
    iconCen: {
        fontSize: 14,
    }

})