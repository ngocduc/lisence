import React, { useState, useRef, useEffect, useCallback } from 'react';
import { View, SafeAreaView, FlatList, TouchableOpacity, Dimensions, BackHandler,
     Text, ScrollView, StyleSheet, Alert } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { isEmpty } from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

import Header from './com/header';
import { RenderItem } from './com/exam_item';
import * as jsonData from '../../data/b2/data'
import { getItem, saveItem } from '../../hepper/handleStore';
import { KEY_STORE } from '../../hepper/const';
// import ex1 from '../../data/test1.json';

const { width, height } = Dimensions.get('window');

const Practice = ({ route, ...props }) => {
    const { type = '' } = route.params || {};

    const [index, setIndex] = useState(0);
    const [animation, setAnimation] = useState('fadeIn')
    const [listExam, setListExam] = useState([])
    const [userAnswer, setUserSelect] = useState({})

    useEffect(() => {
        if (type == 'wrong') {
            getItem(KEY_STORE.USER_WRONG)
                .then((data) => {
                    if (data) {
                        console.log('data123123123', data)
                        const dataInput = data.map((item, index) => {
                            item.index = index;
                            return item
                        })
                        setListExam(dataInput)
                    } else {
                        Alert.alert("Vui lòng làm bộ đề để có thống kê", "", [
                            {
                                text: "OK", onPress: () => {
                                    props.navigation.goBack()
                                }
                            }
                        ]
                        )
                    }
                })
        } else {
            const test_id = 1 + Math.floor(Math.random() * 17)
            console.log(test_id, '=====')
            const ex1 = jsonData[`b${test_id}`]
            if (ex1 && ex1.questions) {
                const dataInput = ex1.questions.map((item, index) => {
                    item.index = index;
                    return item
                })
                setListExam(dataInput)
            }
        }
    }, [type])

    useEffect(() => {
        if (index) {
            setAnimation('fadeOut')
            setTimeout(() => {
                setAnimation('fadeIn')
            }, 100)
        }
    }, [index])

    useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', onBackPress);
        return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])

    const onBackPress = () => {
        console.log('-123123123123')
        Alert.alert("Kết thúc bài thi?", "Kết thúc bài thi và không chấm điểm", [
            {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
            },
            { text: "OK", onPress: () => props.navigation.goBack() }
        ])
        return true;
    }


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => { props.navigation.goBack() }}>
                    <IconAnt style={{ fontSize: 20 }} name="down" type="AntDesign" />
                </TouchableOpacity>
            </View>
            <Header index={+index + 1} setIndex={setIndex} total={listExam.length} />
            <Animatable.View animation={animation} style={{ flex: 1, }}>
                {listExam[index] && animation == 'fadeIn' ? <RenderItem
                    item={listExam[index]} index={index}
                    userAnswer={userAnswer[index]}
                    onSubmit={val => setUserSelect({
                        ...userAnswer,
                        ...val,
                    })} /> : null}
            </Animatable.View>
            <NavigateIndex
                index={index}
                handleChangeIndex={setIndex}
                max={listExam.length}
            />
        </SafeAreaView>
    )

}


const fakeData =
{
    id: '123',
    question: 'Khái niệm "đường bộ" được hiểu thê snaof cho đúng? Đường bộ bao gồm:',
    img: ['link',],
    listAnswer: [
        {
            id: '2o34',
            content: 'Đường cầu đường bộ',
            correct: true,
        },
        {
            id: '2w3o4',
            content: 'Hầm đường bộ, phà đường bộ',
            correct: true,
        },
        {
            correct: false,
            id: '2o3w4',
            content: 'Đường cầu đường bộ hầm đường bộ, bến phà đường bộ và các công trình đường bộ phụ trợ khác'
        },
    ],
    answer: "Đường bộ thì chỉ liên quan đến đường không bao gồm công trình phụ trợ"
}


export default Practice;

const ListAn = ["A", "B", "C", "D", "E"]



export const NavigateIndex = ({ handleChangeIndex, index, max, openMod }) => {
    return (
        <View style={stNavigate.container}>
            {index != 0 ?
                <TouchableOpacity
                    onPress={() => handleChangeIndex(+index - 1)}
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon type='MaterialCommunityIcons' name="chevron-left" style={{ color: '#A87A29', fontWeight: 'bold' }} />
                    <Text style={[stNavigate.text, { color: '#A87A29', fontWeight: 'bold' }]}> Trước </Text>
                </TouchableOpacity>
                : <View />}

            {+index != +max - 1 ?
                <TouchableOpacity
                    onPress={() => handleChangeIndex(+index + 1)}
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Text style={[stNavigate.text, { color: '#A87A29', fontWeight: 'bold' }]}>Tiếp theo </Text>
                    <Icon type='MaterialCommunityIcons' name="chevron-right" style={{ color: '#A87A29', fontWeight: 'bold' }} />
                </TouchableOpacity> :
                <View />
            }

        </View>
    )
}


const stNavigate = StyleSheet.create({
    container: {
        width: '100%',
        // backgroundColor: '#dedede',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        // borderTopColor: '#dedede',
        // borderTopWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    text: {
        fontSize: 18,
    },
    leftBtn: {
        // opacity: index == 1? 0.5: 1,
        // width: "40%",
        justifyContent: "center",
        alignItems: "center",

        // backgroundColor: 'red',
        // borderRadius: 25,
        // borderWidth: 4,
        paddingVertical: 10,
        // borderColor: '#e88812'
    },
    centerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "60%",
        height: '100%'
    },
    // rightBtn: {
    // 	width: "40%",
    // },
    next: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        paddingVertical: 12
    },
    iconCen: {
        fontSize: 14,
    }

})