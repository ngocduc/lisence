import React, { useEffect, useState, useRef } from 'react';

import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
const TIME = 22;

const CountDownHeader = ({ time = TIME, _handleEnd = () => { }, endGame }) => {
    const [count, setCount] = useState(time * 60);
    const [delay, setDelay] = useState(1000);

    useEffect(() => {
        if (endGame) {
            setDelay(null)
        } else {
            setCount(TIME * 60)
            setDelay(1000)
        }
    }, [endGame])

    useInterval(() => {
        setCount(count - 1)
    }, delay)

    useEffect(() => {
        if (count == 0) {
            _handleEnd({ force: true });
            setDelay(null)
        }
    }, [count])

    return (
        <View style={styles.container}>
            <Text style={styles.time}>{endGame ? '' : convertTime(count)}</Text>
            <TouchableOpacity onPress={() => _handleEnd({ count })}>
                <Text style={styles.text}>{endGame ? "Xem kết quả" : "Chấm điểm"}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: { flexDirection: 'row', justifyContent: 'space-between',
     alignItems: 'center', paddingHorizontal: 15, marginTop: 10
     },
    time: { fontSize: 18, fontWeight: '500' },
    text: { color: '#74A0B8', fontSize: 18, fontWeight: '500' }

})

function useInterval(callback, delay) {
    const savedCallback = useRef();

    // Remember the latest callback.
    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    // Set up the interval.
    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}

export default CountDownHeader;

const convertTime = (second) => {
    try {
        return `${(second - second % 60) / 60}:${second % 60 >= 10 ? (second % 60) : `0${second % 60}`}`
    } catch (err) {
        return second
    }
}