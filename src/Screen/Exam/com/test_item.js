import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
    View, SafeAreaView, FlatList, TouchableOpacity, Image,
    Dimensions, Text, ScrollView, StyleSheet, Platform
} from 'react-native';
// import * as Animatable from 'react-native-animatable';
// import { isEmpty } from 'lodash';
// import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
// import IconMaterial from 'react-native-vector-icons/MaterialIcons';


const { width, height } = Dimensions.get('window');
export const RenderItem = ({ item, onSubmit = () => { }, userAnswer = [], index, isShow = false }) => {
    const [selectItem, setSelectItem] = useState([]);
    useEffect(() => {
        setSelectItem(userAnswer);
    }, [index])

    const _handleSelect = useCallback((indexItem) => {
        if (selectItem.includes(indexItem)) {
            setSelectItem(selectItem.filter(i => i !== indexItem))
            onSubmit({
                [index]: selectItem.filter(i => i !== indexItem)
            })
        } else {
            setSelectItem([...selectItem, indexItem])
            onSubmit({
                [index]: [...selectItem, indexItem]
            })
        }
    }, [selectItem])

    return (

        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <ScrollView style={{ flex: 1, paddingHorizontal: 8 }}>
                <Text style={itemStyles.headerQuestion}>{item.title}</Text>
                <View>
                    {
                        item.images && item.images[0] ? item.images.map(img => {
                            return (
                                <View key={img} style={{ height: 150, width: width, marginTop: 10 }}>
                                    <Image resizeMode="contain" source={{ uri: img }} style={{ flex: 1 }} />
                                </View>
                            )
                        }) : null
                    }
                </View>
                {
                    item.listAnswer.map((an, indexAn) => {
                        return (
                            <View key={an.id}>
                                <TouchableOpacity
                                    onPress={() => isShow ? null : _handleSelect(indexAn)}
                                    style={[
                                        itemStyles.answerItem,
                                        {
                                            borderColor: handleColor({ correct: an.correct, select: selectItem.includes(indexAn), isShow }),
                                            backgroundColor: handleBackgroundColor({ correct: an.correct, select: selectItem.includes(indexAn), isShow })
                                        }]}>
                                    <Text style={itemStyles.anItem}><Text style={{ color: 'red', fontWeight: 'bold' }}>{ListAn[indexAn]}.</Text> {an.content}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
                {(isShow) ? <View style={itemStyles.answer}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconAnt name="tagso" style={{ fontSize: 20 }} />
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Giải thích và đáp án</Text>
                    </View>
                    <Text>asfasdf</Text>
                    <Text style={{ fontSize: 17, marginTop: 10 }}>{item.answer}</Text>

                </View> : null}
            </ScrollView>
        </View>
    )
}


const handleColor = ({ correct, select, isShow }) => {
    if (isShow) {
        if (correct) return "green"
        if (select) return "red"
    } else {
        if (select) return "#FEB92C"
        else return '#ddd'
    }
}
const handleBackgroundColor = ({ correct, select, isShow }) => {
    if (isShow) {
        if (correct) return 'rgba(107, 188, 146, 0.5)'
        if (select) return 'rgba(235, 146, 122, 0.9)'
    } else {
        if (select) return '#efefef'
        return '#fff'
    }
}

const itemStyles = StyleSheet.create({
    headerQuestion: {
        fontSize: 19,
        fontFamily: "Eligible Bold",
        // fontFamily: Platform.OS === 'ios' ? 'vincHand' : 'sans-serif-light',
        // fontFamily: Platform.OS === 'ios' ? 'Cochin' : 'Montserrat-Regular',
    },
    answerItem: {
        marginTop: 10,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: '#aaa',
        paddingHorizontal: 10,
        paddingVertical: 8
    },
    anItem: {
        // fontFamily: "Eligible Bold",
        fontSize: 18
    },
    errText: {
        color: '#FD676A'
    },
    answer: {
        marginTop: 10,
        backgroundColor: 'rgba(107, 188, 146, 0.5)',
        borderRadius: 8,
        paddingHorizontal: 10,
        paddingVertical: 10
    }
})


const fakeData =
{
    id: '123',
    question: 'Khái niệm "đường bộ" được hiểu thê snaof cho đúng? Đường bộ bao gồm:',
    img: ['link',],
    listAnswer: [
        {
            id: '2o34',
            content: 'Đường cầu đường bộ',
            correct: true,
        },
        {
            id: '2w3o4',
            content: 'Hầm đường bộ, phà đường bộ',
            correct: true,
        },
        {
            correct: false,
            id: '2o3w4',
            content: 'Đường cầu đường bộ hầm đường bộ, bến phà đường bộ và các công trình đường bộ phụ trợ khác'
        },
    ],
    answer: "Đường bộ thì chỉ liên quan đến đường không bao gồm công trình phụ trợ"
}


const ListAn = ["A", "B", "C", "D", "E"]


