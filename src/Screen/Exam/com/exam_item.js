import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
    View, SafeAreaView, FlatList, TouchableOpacity, Dimensions,
    Platform,
    Text, ScrollView, StyleSheet
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { isEmpty } from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';


export const RenderItem = ({ item, onSubmit = () => { }, userAnswer, index }) => {
    const [selectItem, setSelectItem] = useState([]);
    const [show, setShow] = useState(false);

    useEffect(() => {
        if (userAnswer) {
            setSelectItem(userAnswer);
            setShow(true)

        } else {
            setSelectItem([]);
            setShow(false)
        }
    }, [index])

    const _handleSelect = useCallback((indexItem) => {
        if (show) return 0
        if (selectItem.includes(indexItem)) {
            const newVal = selectItem.filter(i => i !== indexItem);
            setSelectItem(newVal)
            onSubmit({
                [index]: newVal
            })
        } else {
            const newVal = [...selectItem, indexItem];
            setSelectItem(newVal)
            onSubmit({
                [index]: newVal
            })
        }
    }, [selectItem, show])

    const _handleSubmit = () => {
        setShow(true)
    }


    return (

        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <ScrollView style={{ flex: 1, paddingHorizontal: 8 }}>
                <Text style={itemStyles.headerQuestion}>{item.title}</Text>
                {
                    item.listAnswer.map((an, indexAn) => {
                        return (
                            <View key={an.id}>
                                <TouchableOpacity
                                    onPress={() => _handleSelect(indexAn)}
                                    style={[itemStyles.answerItem, {
                                        borderColor: handleColor({ correct: an.correct, select: selectItem.includes(indexAn), show }),
                                        backgroundColor: handleBackgroundColor({ correct: an.correct, select: selectItem.includes(indexAn), show }),
                                    }]}>
                                    <Text style={itemStyles.anItem}><Text style={{ color: 'red', fontWeight: 'bold' }}>{ListAn[indexAn]}.</Text> {an.content}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                    <IconMaterial type="MaterialIcons" name='report' style={{ color: '#FD676A', fontSize: 20 }} />
                    <Text style={itemStyles.errText}>Báo cáo lỗi</Text>
                </TouchableOpacity>

                {(show) ? <View style={itemStyles.answer}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconAnt name="tagso" style={{ fontSize: 20 }} />
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Giải thích và đáp án</Text>
                    </View>
                    <Text style={{ fontSize: 17, marginTop: 10 }}>{item.answer}</Text>

                </View> : null}
            </ScrollView>
            {show || isEmpty(selectItem) ? null : (
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={_handleSubmit} style={{ backgroundColor: '#2C9E62', borderRadius: 10, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 15, paddingVertical: 6 }}>
                        <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }}>Kiểm tra</Text>
                    </TouchableOpacity>
                </View>
            )}
        </View>
    )
}


const handleColor = ({ correct, select, show }) => {
    if (show) {
        if (correct) return "green"
        if (select) return "red"
    } else {
        if (select) return "#FEB92C"
        else return '#aaa'
    }
}

const handleBackgroundColor = ({ correct, select, show }) => {
    if (show) {
        if (correct) return 'rgba(107, 188, 146, 0.5)'
        if (select) return 'rgba(235, 146, 122, 0.9)'
    } else {
        if (select) return '#efefef'
        return '#fff'
    }
}
const itemStyles = StyleSheet.create({
    headerQuestion: {
        fontSize: 22,
        fontFamily: "Eligible Bold"
    },
    answerItem: {
        marginTop: 10,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: '#aaa',
        paddingHorizontal: 10,
        paddingVertical: 8
    },
    anItem: {
        // fontFamily: 'Roboto',
        fontSize: 17
    },
    errText: {
        color: '#FD676A'
    },
    answer: {
        marginTop: 10,
        backgroundColor: 'rgba(107, 188, 146, 0.5)',
        borderRadius: 8,
        paddingHorizontal: 10,
        paddingVertical: 10
    }
})


const fakeData =
{
    id: '123',
    question: 'Khái niệm "đường bộ" được hiểu thê snaof cho đúng? Đường bộ bao gồm:',
    img: ['link',],
    listAnswer: [
        {
            id: '2o34',
            content: 'Đường cầu đường bộ',
            correct: true,
        },
        {
            id: '2w3o4',
            content: 'Hầm đường bộ, phà đường bộ',
            correct: true,
        },
        {
            correct: false,
            id: '2o3w4',
            content: 'Đường cầu đường bộ hầm đường bộ, bến phà đường bộ và các công trình đường bộ phụ trợ khác'
        },
    ],
    answer: "Đường bộ thì chỉ liên quan đến đường không bao gồm công trình phụ trợ"
}


const ListAn = ["A", "B", "C", "D", "E"]


