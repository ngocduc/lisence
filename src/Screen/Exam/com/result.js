import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import IconAnt from 'react-native-vector-icons/AntDesign';
import { Button } from 'react-native-paper'
import { NUMBER } from '../../../hepper/const';

const { width, height } = Dimensions.get('window');
const convertResult = (result) => {

    let correct = 0
    let inCorrect = 0
    Object.keys(result).map(key => {
        if (result[key] === true) {
            correct += 1
        }
        if (result[key] === false) {
            inCorrect += 1
        }
    })
    return {
        correct, inCorrect
    }
}

const Result = ({
    count = 0, listExam, navigation, result,
    _handlePressItem = () => { },
    _handleReset = () => { },
}) => {
    const [state, setState] = useState('all');
    const [dataShow, setDataShow] = useState({});
    const [dataRender, setDataRender] = useState(listExam);

    useEffect(() => {
        const { correct, inCorrect } = convertResult(result)

        setDataShow({
            correct, inCorrect,
            unCheck: listExam.length - correct - inCorrect,
            text: correct >= NUMBER.QUALIFIED ? 'Đạt' : "Không đạt",
            bg: correct >= NUMBER.QUALIFIED ? '#9FE381' : "rgba(235, 146, 122, 0.9)",
            color: correct >= NUMBER.QUALIFIED ? 'green' : "red",
        })
    }, [result, listExam])

    useEffect(() => {
        if (state == 'all') {
            setDataRender(listExam)

        } else if (state == 'correct') {
            const dateNew = listExam.filter((i, index) => {
                return (result[index] === true)
            })
            setDataRender(dateNew)

        } else if (state == 'inCorrect') {
            const dateNew = listExam.filter((i, index) => {
                return (result[index] === false)
            })
            setDataRender(dateNew)

        } else {
            const dateNew = listExam.filter((i, index) => {
                return (result[index] === undefined)
            })
            setDataRender(dateNew)

        }

    }, [state])
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', paddingVertical: 15, alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 10 }}>
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <IconAnt style={{ fontSize: 25 }} name="left" type="AntDesign" />
                </TouchableOpacity>
                <Text style={{ fontSize: 25 }}>Kết quả</Text>
                <View style={{ width: 25 }} />
            </View>
            <View style={{ backgroundColor: dataShow.bg, padding: 5, marginHorizontal: 7, borderRadius: 10, alignItems: 'center', borderColor: 'red', borderWidth: 1 }}>
                <Text style={{ color: dataShow.color, fontSize: 20, fontWeight: '500' }}>{dataShow.text}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10 }}>
                <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                    <IconAnt style={{ fontSize: 18, color: '#5987D3' }} name="clockcircleo" />
                    <Text style={{ fontSize: 18, marginLeft: 10, color: '#5987D3', fontWeight: 'bold' }}>{convertTime(count)}</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                    <IconAnt style={{ fontSize: 18, color: '#9FE381' }} name="checksquareo" />
                    <Text style={{ fontSize: 18, marginLeft: 10, color: 'green', fontWeight: 'bold' }}>{dataShow.correct}/{listExam.length}</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                <TouchableOpacity onPress={() => setState('all')} style={[styles.itemFilter, { borderBottomColor: state == 'all' ? '#5987D3' : '#fff' }]}>
                    <IconAnt style={{ fontSize: 17, marginBottom: 5, marginRight: 7 }} name="form" />
                    <Text style={{ fontSize: 20, marginBottom: 5, }}>{listExam.length}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setState('correct')}
                    style={[styles.itemFilter, { borderBottomColor: state == 'correct' ? '#5987D3' : '#fff' }]}>
                    <IconAnt style={{ fontSize: 17, marginBottom: 5, marginRight: 7, color: 'green' }} name="checkcircleo" />
                    <Text style={{ fontSize: 20, marginBottom: 5, }}>{dataShow.correct}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setState('inCorrect')}
                    style={[styles.itemFilter, { borderBottomColor: state == 'inCorrect' ? '#5987D3' : '#fff' }]}>
                    <IconAnt style={{ fontSize: 17, marginBottom: 5, marginRight: 7, color: 'red' }} name="closecircleo" />
                    <Text style={{ fontSize: 20, marginBottom: 5, }}>{dataShow.inCorrect}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setState('unCheck')}
                    style={[styles.itemFilter, { borderBottomColor: state == 'unCheck' ? '#5987D3' : '#fff' }]}>
                    <IconAnt style={{ fontSize: 17, marginBottom: 5, marginRight: 7, color: '#A87A29' }} name="warning" />
                    <Text style={{ fontSize: 20, marginBottom: 5, }}>{dataShow.unCheck}</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={dataRender}
                keyExtractor={(item, index) => String(item.index)}
                numColumns={5}
                style={{ paddingLeft: 10, paddingBottom: 20 }}
                extraData={dataRender}

                renderItem={({ item, index }) => {
                    const {
                        color, backgroundColor, icon
                    } = convertIcon(result[item.index])
                    return (
                        <TouchableOpacity onPress={() => _handlePressItem(item.index)}
                            style={[styles.itemRender, { backgroundColor, borderColor: color }]}>
                            <IconAnt name={icon} style={{ color, fontSize: 18, marginBottom: 6 }} />
                            <Text style={styles.textItem}>Câu {item.index + 1}</Text>
                        </TouchableOpacity>
                    )
                }}
            />
            <View style={{
                flexDirection: 'row', justifyContent: 'space-around',
                paddingVertical: 10, marginBottom: 15
            }}>
                <Button onPress={() => navigation.goBack()} color="red"><Text>Thoát</Text></Button>
                <Button onPress={_handleReset} mode="contained"><Text style={{ fontWeight: 'bold' }}>Làm lại</Text></Button>
            </View>
        </View>

    )
};

const convertIcon = (isCorrect) => {
    if (isCorrect === true) {
        return { icon: 'checkcircleo', color: 'green', backgroundColor: '#9FE381' }
    }
    if (isCorrect === false) {
        return { icon: 'closecircleo', color: 'red', backgroundColor: 'rgba(235, 146, 122, 0.9)' }
    }
    return {
        icon: 'warning', color: '#A87A29', backgroundColor: '#efefef'
    }
}

const styles = StyleSheet.create({
    itemFilter: {
        flex: 1, flexDirection: 'row', justifyContent: 'center', marginHorizontal: 15,
        alignItems: 'center', borderBottomWidth: 3
    },
    itemRender: {
        // flex: 1, 
        width: (width - 60) / 5,
        marginRight: 10,
        justifyContent: 'center', alignItems: 'center',
        marginTop: 10, aspectRatio: 1, borderRadius: 8, padding: 8,
        borderWidth: 1,
    },
    textItem: {
        fontSize: 12
    }

})

const convertTime = (second) => {
    try {
        return `${(second - second % 60) / 60}:${second % 60 > 9 ? (second % 60) : `0${second % 60}`}`
    } catch (err) {
        return second
    }
}
export default Result;