import React, { useState, useRef, useEffect } from 'react';
import { View, SafeAreaView, FlatList, TouchableOpacity, Dimensions, Text } from 'react-native';

const { width, height } = Dimensions.get('window');

const Header = ({ index = 0, setIndex = () => { }, total, userAnswer = {} }) => {
    const flatRef = useRef(null);

    useEffect(() => {
        handleScrollFlatList(index - 1)
    }, [index]);

    const handleScrollFlatList = (indexScroll = 10) => {
        if (flatRef.current && flatRef.current.scrollToOffset) {
            flatRef.current.scrollToOffset({ animated: true, offset: indexScroll * 60 });
        }
    }
    return (
        <View style={{
            // height: 60,
            borderBottomColor: '#dedede',
            borderBottomWidth: 1,
            marginBottom: 15
        }}>
            <FlatList
                showsHorizontalScrollIndicator={false}
                ref={flatRef}
                contentContainerStyle={{ alignItems: 'center' }}
                horizontal={true}
                // data={[null, ...new Array(dataCourseConvert.length).fill('').map((_, i) => i + 1), null]}
                data={[null, ...new Array(total).fill('').map((_, i) => i + 1), null]}
                renderItem={({ item, index: indexItem }) => {
                    const isCurrent = item == index;
                    const isActive = userAnswer[indexItem]
                    if (item === null) {
                        return (
                            <View style={{ width: width / 2 - 30, height: 10 }} />
                        )
                    }
                    else return (
                        <TouchableOpacity
                            onPress={() => {
                                setIndex(`${item-1}`)
                            }}
                            style={{
                                height: 60,
                                width: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <Text style={{
                                color: isCurrent ? '#007ACC' : isActive ? 'green':'#000',
                                fontSize: isCurrent ? 21 : 17,
                                fontWeight: isCurrent ? "bold" : '400'
                            }}>
                                {item}
                            </Text>
                        </TouchableOpacity>
                    );
                }}
                keyExtractor={(item, index) => index + 'subitem'}
            />
            <View style={{ height: 2, width: 60, backgroundColor: '#007ACC', marginLeft: width / 2 - 30 }} />
        </View>

    )
}

export default Header;