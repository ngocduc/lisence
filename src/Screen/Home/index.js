import React, { useEffect } from 'react';
import {
    View, SafeAreaView, FlatList, Text, TouchableOpacity,
    Dimensions,
} from 'react-native'
import { Button, IconButton } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import Realm from "realm";

import Live from '../../com/LiveStream';

const TaskSchema = {
    name: "Task",
    properties: {
        _id: "int",
        name: "string",
        status: "string?",
    },
    primaryKey: "_id",
};



const { width, height } = Dimensions.get('window');
const Home = (props) => {

    useEffect(() => {
        // _handleReal()
    }, [])

    const _handleReal = async () => {

        const realm = await Realm.open({
            path: "myrealm",
            schema: [TaskSchema],
        });

        // query realm for all instances of the "Task" type.
        const tasks = realm.objects("Task");
        const openTasks = tasks.filtered("status = 'Open'");
        console.log('openTasks', openTasks, tasks)

        // Add a couple of Tasks in a single, atomic transaction
        // let task1, task2;
        realm.write(() => {
            // task1 = realm.create("Task", {
            //     _id: 1,
            //     name: "go grocery shopping",
            //     status: "Open",
            // });
            // task2 = realm.create("Task", {
            //     _id: 2,
            //     name: "go exercise",
            //     status: "Open",
            // });
            // tasks[0].status = "dnd"
            const data = openTasks.map(t => {
                t.status = '23412341234'
                // t.status = 'dnd'
                console.log('rrrrr', t)
            });
            console.log('dddd', data)
            // console.log(`created two tasks: ${task1.name} & ${task2.name}`);
        });
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Live navigation={props.navigation} />
            <FlatList
                style={{}}
                keyExtractor={(_, index) => 'book_item' + index.toString()}
                numColumns={2}
                data={listOption}
                renderItem={({ item, index }) => {
                    return (
                        <LinearGradient
                            start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                            colors={item.colors}
                            style={{
                                width: width / 2 - 20,
                                margin: 10,
                                backgroundColor: 'green',
                                borderRadius: 10,
                                padding: 10,
                                aspectRatio: 3 / 2
                            }} >
                            <TouchableOpacity onPress={() => {
                                if ('Sa hình' !== item.name) {
                                    props.navigation.navigate(item.navigate, item.data || {})
                                }
                            }
                            } style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <IconButton
                                    icon={item.icon}
                                    color={'#fff'}
                                    size={35} />
                                <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold', marginTop: 10 }}>{item.name}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    )
                }}
            />

        </SafeAreaView>
    )
};

const listOption = [
    {
        name: 'Đề ngẫu nhiên',
        icon: 'swap-horizontal-bold',
        type: 'FontAwesome',
        navigate: 'Exam',
        colors: ['#EF8263', '#EB778C']
    },
    {
        name: 'Thi theo bộ đề',
        icon: 'file-document',
        type: 'Entypo',
        navigate: 'ListExam',
        colors: ['#FC7B50', '#FCD766']
    },
    {
        name: 'Xem câu bị sai',
        icon: 'close-circle-outline',
        type: 'AntDesign',
        navigate: 'Practice',
        colors: ['#A58EE1', '#9079D1'],
        data: {
            type: 'wrong'
        }
    },
    // {
    //     name: 'Ôn tập câu hỏi',
    //     icon: 'book-open-page-variant',
    //     type: 'Entypo',
    //     navigate: 'Exam',
    //     colors: ['#89B771', '#B1D876']
    // },
    {
        name: 'Mẹo ghi nhớ',
        icon: 'tag-multiple',
        type: 'AntDesign',
        navigate: 'Note',
        colors: ['#E284B8', '#CE6EA6']
    },
    // {
    //     name: 'Các biển báo',
    //     icon: 'traffic-light',
    //     type: 'FontAwesome5',
    //     navigate: 'TrafficSigns',
    //     colors: ['#5C98E1', '#4B86D1']
    // },
    // {
    //     name: 'Sa hình',
    //     icon: 'car',
    //     type: 'AntDesign',
    //     navigate: 'Exam',
    //     colors: ['#52B9DD', '#40A8D0']
    // },
    // {
    //     name: 'Top câu sai',
    //     icon: 'chart-gantt',
    //     type: 'Entypo',
    //     navigate: 'Exam',
    //     colors: ['#E25463', '#D04353']
    // },
]

export default Home