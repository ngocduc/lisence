import React from 'react';
import { View, Text, SafeAreaView, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { DataTable } from 'react-native-paper';

const { width, height } = Dimensions.get('window');

const Note = () => {
    return (
        <SafeAreaView style={{ flex: 1, marginHorizontal: 10, paddingTop: 20 }}>
            <ScrollView>
                <Text style={styles.h1}>I. CÁC KHÁI NIỆM CƠ BẢN</Text>
                <Text style={styles.h3}>1. Phần đường xe chạy: Là phần của đường bộ được sử dụng cho các phương tiện giao thông qua lại.</Text>
                <Text style={styles.h3}>
                    2. Làn đường: chia theo chiều dọc, có bề rộng đủ cho xe chạy an toàn </Text>

                <Text style={styles.h3}>3. Khổ giới hạn đường bộ: chiều cao, chiều rộng, hàng hóa xếp trên xe.</Text>

                <Text style={styles.h3}>4. Dải phân cách: phân chia phần đường xe cơ giới, xe thô sơ.  </Text>

                <Text style={styles.h3}>Gồm hai loại: cố định và di động.</Text>

                <Text style={styles.h3}>5. Người lái xe: là người điều khiển xe cơ giới.</Text>

                <Text style={styles.h3}>6. Đường ưu tiên: được các phương tiện nhường đường.</Text>

                <Text style={styles.h3}>7. Phương tiện giao thông cơ giới đường bộ: kể cả xe máy điện, các loại xe tương tự.</Text>

                <Text style={styles.h3}>8. Phương tiện giao thông thô sơ đường bộ: kể cả xe đạp máy, các loại xe tương tự.</Text>

                <Text style={styles.h3}>9. Phương tiện tham gia giao thông đường bộ: cơ giới, thô sơ, xe máy chuyên dùng.</Text>

                <Text style={styles.h3}>10. Người điều khiển phương tiện tham gia giao thông: người điều khiển xe cơ giới, xe thô sơ, xe máy chuyên dùng.</Text>

                <Text style={styles.h3}>11. Người tham gia giao thông:</Text>

                <Text style={styles.h3}>+ Người sử dụng phương tiện.</Text>

                <Text style={styles.h3}>+ Dẫn dắt súc vật, người đi bộ.</Text>

                <Text style={styles.h3}>12. Người điều khiển giao thông:</Text>

                <Text style={styles.h3}>+ Cảnh sát giao thông.</Text>

                <Text style={styles.h3}>+ Người được giao nhiệm vụ hướng dẫn giao thông.</Text>

                <Text style={styles.h3}>13. Dừng xe: đứng yên tạm thời.</Text>

                <Text style={styles.h3}>14. Đổ xe: đứng yên không giới hạn thời gian.</Text>

                <Text style={styles.h1}>II. QUY TẮC GIAO THÔNG ĐƯỜNG BỘ</Text>
                <Text style={styles.h3}>1. Hiệu lệnh của người điều khiển giao thông:</Text>

                <Text style={styles.h3}>2. Tay giơ thẳng đứng: tất cả dừng lại.</Text>

                <Text style={styles.h3}>3. Hai tay hoặc một tay giơ ngang: trước sau dừng lại, trái phải được đi.</Text>

                <Text style={styles.h3}>4. Khi sử dụng GPLX đã khai báo mất: bị tước GPLX 5 năm.</Text>

                <Text style={styles.h3}>5. Trong đô thị và khu đông dân cư:</Text>

                <Text style={styles.h3}>+ Không được bấm còi từ 22 giờ tối đến 5 giờ sáng.</Text>

                <Text style={styles.h3}>+ Không được sử dụng đèn chiếu xa.</Text>

                <Text style={styles.h3}>+ Chỉ được báo hiệu xin vượt bằng đèn từ 22 giờ đến 5 giờ sáng.</Text>

                <Text style={styles.h3}>+ Chỉ được quay đầu xe nơi đường giao nhau, nơi có biển báo quay đầu xe.</Text>

                <Text style={styles.h3}>6. Nhường đường tại nơi giao nhau.</Text>

                <Text style={styles.h3}>+ Không có biển báo hiệu đi theo vòng xuyến nhường đường bên phải.</Text>

                <Text style={styles.h3}>+ Có biển báo hiệu đi theo vòng xuyến nhường đường bên trái.</Text>

                <Text style={styles.h3}>7. Xe quá tải trọng, quá khổ khi lưu thông trên đường: phải được cơ quan quản lý đường bộ có thẩm quyền cấp phép.</Text>

                <Text style={styles.h3}>8. Khi đỗ xe ô tô sát lề đường hè phố phái bên phải:</Text>

                <Text style={styles.h3}>+ Cách lề, hè phố không quá 0.25 m.</Text>

                <Text style={styles.h3}>+ Cách xe ô tô đang đỗ phía bên kia đường tối thiểu 20m.</Text>

                <Text style={styles.h3}>9. Vạch nét liền không được đè vạch, vạch nét đứt được đè vạch</Text>

                <Text style={styles.h3}>10. Vạch màu vàng phân chia các làn xe ngược chiều; vạch màu trắng phân chia các làn xe chạy cùng chiều.</Text>

                <Text style={styles.h1}>III. TỐC ĐỘ TỐI ĐA CHO PHÉP, KHOẢNG CÁCH AN TOÀN</Text>

                <Text style={styles.h2}>- Xe máy chuyên dùng, xe gắn máy (kể cả xe máy điện) và các loại xe tương tự : 40 km/h</Text>
                <Text style={styles.h2}>- Tốc độ tối đa cho phép trong khu vực đông dân cư (trừ đường cao tốc)</Text>
                <ScrollView horizontal>
                    <View style={{ width: width * 1.5, marginVertical: 10 }}>
                        {/* header */}
                        <View style={{ flexDirection: 'row', width: width * 1.5 }}>
                            <View style={[styles.border, { flex: 1 }]}><Text style={styles.textCenter}>Loại xe cơ giới đường bộ</Text></View>
                            <View style={{ flex: 2 }}>
                                <View style={[styles.border, { flex: 1 }]}>
                                    <Text style={styles.textCenter}>Tốc độ tối đa (km/h)</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1 }}>
                                    <View style={[styles.border, { flex: 1 }]}>
                                        <Text style={styles.textCenter} numberOfLines={3}>Đường đôi; đường một chiều có từ hai làn xe cơ giới trở lên</Text>
                                    </View>
                                    <View style={[styles.border, { flex: 1 }]}>
                                        <Text style={styles.textCenter} numberOfLines={3}>Đường hai chiều; đường một chiều có một làn xe cơ giới</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/*  body */}
                        <View style={{ flexDirection: 'row' }}>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>Các phương tiện xe cơ giới, trừ xe máy chuyên dùng, xe gắn máy (kể cả xe máy điện) và các loại xe tương tự</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>60</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>50</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <Text style={styles.h2}>- Tốc độ tối đa cho phép ngoài khu vực đông dân cư (trừ đường cao tốc)</Text>
                <ScrollView horizontal>
                    <View style={{ width: width * 1.5, marginVertical: 10 }}>
                        {/* header */}
                        <View style={{ flexDirection: 'row', width: width * 1.5 }}>
                            <View style={[styles.border, { flex: 1 }]}><Text style={styles.textCenter}>Loại xe cơ giới đường bộ</Text></View>
                            <View style={{ flex: 2 }}>
                                <View style={[styles.border, { flex: 1 }]}>
                                    <Text style={styles.textCenter}>Tốc độ tối đa (km/h)</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1 }}>
                                    <View style={[styles.border, { flex: 1 }]}>
                                        <Text style={styles.textCenter} numberOfLines={3}>Đường đôi; đường một chiều có từ hai làn xe cơ giới trở lên</Text>
                                    </View>
                                    <View style={[styles.border, { flex: 1 }]}>
                                        <Text style={styles.textCenter} numberOfLines={3}>Đường hai chiều; đường một chiều có một làn xe cơ giới</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/*  body */}
                        <View style={{ flexDirection: 'row' }}>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>Xe ô tô con, xe ô tô chở người đến 30 chỗ (trừ xe buýt); ô tô tải có trọng tải nhỏ hơn hoặc bằng 3,5 tấn.</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>90</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>80</Text>
                            </View>
                        </View>
                        {/*  */}

                        <View style={{ flexDirection: 'row' }}>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>Xe ô tô chở người trên 30 chỗ (trừ xe buýt); ô tô tải có trọng tải trên 3,5 tấn (trừ ô tô xi téc).	</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>80</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>70</Text>
                            </View>
                        </View>
                        {/*  */}

                        <View style={{ flexDirection: 'row' }}>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>Ô tô buýt; ô tô đầu kéo kéo sơ mi rơ moóc; xe mô tô; ô tô chuyên dùng (trừ ô tô trộn vữa, ô tô trộn bê tông).	</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>70</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>60</Text>
                            </View>
                        </View>
                        {/*  */}

                        <View style={{ flexDirection: 'row' }}>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>Ô tô kéo rơ moóc; ô tô kéo xe khác; ô tô trộn vữa, ô tô trộn bê tông, ô tô xi téc.	</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>60</Text>
                            </View>
                            <View style={[styles.border, { flex: 1 }]}>
                                <Text style={styles.textCenter}>50</Text>
                            </View>
                        </View>

                    </View>
                </ScrollView>
                <Text style={styles.h2}>- Khoảng cách an toàn</Text>
                <View style={{ marginVertical: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>Tốc độ lưu hành (km/h)</Text>
                        </View>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>Khoảng cách an toàn tối thiểu (m)</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>V= 60</Text>
                        </View>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>35</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>{"60 < V ≤80	"}</Text>
                        </View>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>55</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>{"80 < V ≤ 100"}</Text>
                        </View>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>70</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>{"100 < V ≤ 120"}</Text>
                        </View>
                        <View style={[styles.border, { flex: 1 }]}>
                            <Text style={styles.textCenter}>100</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.h3}>- Xe cơ giới là chỉ các loại xe ô tô; máy kéo; rơ-moóc hoặc sơ-mi rơ-moóc được kéo bởi xe ôtô; xe máy 2 bánh; xe máy 3 bánh; xe gắn máy (kể cả xe máy điện) và các loại xe tương tự được thiết kế để chở người và hàng hóa trên đường bộ. Xe cơ giới bao gồm cả tàu điện bánh lốp (là loại tàu dùng điện nhưng không chạy trên đường ray).

</Text>
                <Text style={styles.h3}>- Đường đôi là đường mà chiều đi và về được phân biệt bằng dải phân cách (trường hợp phân biệt bằng vạch sơn thì không phải đường đôi).</Text>
                <Text style={styles.h1}>IV. HOẠT ĐỘNG VẬN TẢI ĐƯỜNG BỘ</Text>
                <Text style={styles.h3}>1. Thời gian làm việc vủa người lái xe ô tô: không quá 10h trên ngày, không lái liên tục quá 4h.</Text>

                <Text style={styles.h3}>2. Quyền và nghĩa vụ của hành khách: được miễn phí cước hành lý ≤ 20 kg.</Text>

                <Text style={styles.h3}>3. Hàng siêu trường, siêu trọng: kích thướt hoặc trọng lượng vượt quá giới hạn quy định nhưng không thể tháo rời được.</Text>

                <Text style={styles.h3}>4. Giấy phép lái xe:</Text>

                <Text style={styles.h3}>Hạng A1: xe mô tô 2 bánh có dung tích xy lanh từ 50 cm3 đến dưới 175 cm3, xe mô tô 3 bánh dùng cho người khuyết tật.</Text>

                <Text style={styles.h3}>Hạng A2: xe mô tô 2 bánh có dung tích xy lanh từ 175 cm3 trở lên và hạng A1.</Text>

                <Text style={styles.h3}>Hạng A3: xe mô tô 3 bánh và hạng A1.</Text>

                <Text style={styles.h3}>Hạng A4: máy kéo nhỏ có trọng tải đến 1.000 kg.</Text>

                <Text style={styles.h3}>Hạng B1: chở người đến 9 chổ, tải dưới 3,5 T (không được kinh doanh vận tải).</Text>

                <Text style={styles.h3}>Hạng B1 tự dộng: ô tô tự động chở người đến 9 chổ, tải tự động dưới 3,5 T (không được kinh doanh vận tải), ô tô dùng cho người khuyết tật</Text>

                <Text style={styles.h3}>Hạng B2: chở người đến 9 chổ, tải dưới 3,5 T.</Text>

                <Text style={styles.h3}>Hạng C: chở người đến 9 chổ, tải trên 3,5 T.</Text>

                <Text style={styles.h3}>Hạng D: chở người từ 10-30 chỗ.</Text>

                <Text style={styles.h3}>Hạng E: chở người trên 30 chỗ.</Text>

                <Text style={styles.h3}>Hạng FC: Lái xe hạng C có kéo rơ móc, ô tô đầu kéo kéo sơ mi rơ moóc.</Text>

                <Text style={styles.h3}>Hạng FE: Lái xe hạng E có kéo theo rơ móc, ô tô chở khách nối toa.</Text>

                <Text style={styles.h3}>Tuổi sức khỏe của người lái xe</Text>

                <Text style={styles.h3}>Đủ 16 tuổi: lái xe gắn máy dưới 50 cm3­.</Text>

                <Text style={styles.h3}>Đủ 18 tuổi: lái xe hạng A1, A2, A3, A4, B1, B2.</Text>

                <Text style={styles.h3}>Đủ 21 tuổi: lái xe hạng C.</Text>

                <Text style={styles.h3}>Đủ 24 tuổi: lái xe hạng D.</Text>

                <Text style={styles.h3}>Đủ 27 tuổi: lái xe hạng E.</Text>

                <Text style={styles.h3}>Tuổi tối đa của người lái xe ô tô chở người trên 30 chổ: Nam 55 tuổi, Nữ 50 tuổi.</Text>
                {/*  */}
                <Text style={styles.h1}>V. CẤU TẠO VÀ CÔNG DỤNG CỦA ĐỘNG CƠ Ô TÔ</Text>
                <Text style={styles.h3}>1. Yêu cầu kính chắn gió ô tô: loại kính an toàn.</Text>

                <Text style={styles.h3}>2. Âm lượng còi điện lắp trên ô tô: từ 90-115 dB.</Text>

                <Text style={styles.h3}>3. Nguyên nhân làm động cơ diezen không nổ: Hết nhiên liệu, lõi lọc nhiên liệu bị tắc, lọc khí bị tắc, nhiên liệu lẫn không khí.</Text>

                <Text style={styles.h3}>4. Yêu cầu về an toàn kỹ thuật đối với dây đai an toàn lắp trên ô tô: Cơ cấu hãm giữ chặt dây khi giật đột ngột.</Text>

                <Text style={styles.h3}>5. Động cơ 4 kỳ: 4 hành trình</Text>

                <Text style={styles.h3}>6. Động cơ 2 kỳ: 2 hành trình</Text>

                <Text style={styles.h3}>7. Công dụng dầu bôi trơn: bôi trơn cho các chi tiết của động cơ.</Text>

                <Text style={styles.h3}>8. Công dụng của động cơ ô tô: nhiệt năng biến thành cơ năng.</Text>

                <Text style={styles.h3}>9. Công dụng của hệ thống truyền lực ô tô: truyền mô men quay.</Text>

                <Text style={styles.h3}>10. Công dụng ly hợp của ô tô: truyền hoặc ngắt truyền động.</Text>

                <Text style={styles.h3}>11. Công dụng của hộp số ô tô: đảm bảo cho ô tô chuyển động lùi.</Text>

                <Text style={styles.h3}>12. Công dụng hệ thống lái của ô tô: thay đổi hướng.</Text>

                <Text style={styles.h3}>13. Công dụng hệ thống phanh ô tô: giảm tốc độ.</Text>

                <Text style={styles.h3}>14. Công dụng ác quy: tích trữ điện năng.</Text>

                <Text style={styles.h3}>15. Công dụng máy phát điện: phát điện năng.</Text>
                {/*  */}
                <Text style={styles.h1}>VI. KỸ THUẬT LÁI XE</Text>
                <Text style={styles.h3}>1. Khởi hành ô tô trên đường bằng: đạp ly hợp hết hành trình, vào số 1.</Text>

                <Text style={styles.h3}>2. Khi vào số để khởi hành ô tô có số tự động: đạp phanh chân hết hành trình.</Text>

                <Text style={styles.h3}>3. Sử dụng chân khi điều khiển xe ô tô số tự động: không sử dụng chân trái.</Text>

                <Text style={styles.h3}>4. Khi nhả phanh tay: tay phải bóp khóa hãm.</Text>

                <Text style={styles.h3}>5. Khi điều khiển xe trên đường vòng: giảm tốc độ, về số thấp.</Text>

                <Text style={styles.h3}>6. Khi điều khiển xe ô tô trên đường trơn: không đánh lái ngoặt và phanh gấp.</Text>

                <Text style={styles.h3}>7. Khi điều khiển ô tô gặp mưa to hoặc xương mù: bật đèn chiếu gần và đèn vàng, đi chậm.</Text>

                <Text style={styles.h3}>8. Khi điều khiển ô tô trong trời mưa: Giảm tốc độ, quan sát, không phanh gấp, bật đèn chiếu gần.</Text>

                <Text style={styles.h3}>9. Khi điều khiển ô tô qua đoạn đường ngập nước: Ước lượng độ ngập nước, về số thấp.</Text>

                <Text style={styles.h3}>10. Khi xuống dốc, muốn dừng xe: về số 1, đạp nửa ly hợp cho xe đến chỗ dừng.</Text>

                <Text style={styles.h3}>11. Khi điều khiển ô tô xuống đường dốc dài, độ dốc cao: về số thấp.</Text>

                <Text style={styles.h3}>12. Khi điều khiển ô tô lên dốc cao: về số thấp, đến gần đỉnh dốc đi chậm.</Text>

                <Text style={styles.h3}>13. Khi điều khiển xe ô tô rẽ trái ở chỗ đường giao nhau: có tín hiệu rẽ trái, cho xe chạy chậm tới phía trong của tâm đường giao nhau.</Text>

                <Text style={styles.h3}>14. Khi điều khiển xe ô tô rẽ phải ở chỗ đường giao nhau: điều khiển xe bám sát phía phải, giảm tốc độ.</Text>

                <Text style={styles.h3}>15. Khi quay đầu xe ở nơi nguy hiểm: đưa đầu xe về phía nguy hiểm, đuôi xe về phía an toàn.</Text>

                <Text style={styles.h3}>16. Khi điều khiển ô tô tới gần xe chạy ngược chiều vào ban đêm: đèn chiếu xa sang đèn chiếu gần, nhìn chếch sang phía phải.</Text>

                <Text style={styles.h3}>17. Khi điều khiển xe qua đường sắt: dừng xe tạm thời, quan sát.</Text>

                <Text style={styles.h3}>18. Khi điều khiển xe ô tô tự đổ:</Text>

                <Text style={styles.h3}>  + Không lấy lái gấp và không phanh gấp</Text>

                <Text style={styles.h3}>  + Khi đổ hàng phải chọn vị trí có nền đường cứng và phẳng.</Text>

                <Text style={styles.h3}>19. Khi điều khiển tăng số: không được nhìn xuống buồng lái.</Text>

                <Text style={styles.h3}>20. Khi điều khiển xe giảm số: không được nhìn xuống buồng lái.</Text>

                <Text style={styles.h3}>21. Đổ xe ô tô sát lề đường bên phải, khi mở cửa xe: quan sát tình hình giao thông phía trước, sau, mở hé cánh cửa.</Text>

                <Text style={styles.h3}>22. Kỹ thuật giữ thăng bằng khi điều khiển xe mô tô trên đường gồ ghề: Đứng thẳng trên giá gác chân, hơi gập đầu gối và khủy tay, đi chậm.</Text>

                <Text style={styles.h3}>23. Để đạt hiệu quả phanh cao nhất đối với xe mô tô: Giảm hết ga, sử dụng đồng thời cả phanh sau và phanh trước.</Text>
                {/*  */}
                <Text style={styles.h1}>VIII. BIỂN BÁO VÀ SA HÌNH</Text>
                <Text style={styles.h3}>Thứ tự các xe từ nhỏ đến lớn: ô tô con => khách => tải => máy kéo => ô tô kéo móc => máy kéo kéo móc.</Text>

                <Text style={styles.h3}>Cấm nhỏ thì cấm lớn, cấm lớn không cấm nhỏ.</Text>

                <Text style={styles.h3}>Cấm 2 bánh không cấm 4 bánh, cấm 4 bánh cũng không cấm 2 bánh.</Text>

                <Text style={styles.h2}>- Nguyên tắc xử lý sa hình: Theo thứ tự ưu tiên giảm dần</Text>

                <Text style={styles.h3}> 1. Xe đã vô ngã tư.</Text>
                <Text style={styles.h3}> 2. Xe ưu tiên (hỏa, sự – công, thương).</Text>
                <Text style={styles.h3}> 3. Theo tín hiệu đèn giao thông.</Text>
                <Text style={styles.h3}> 4. Theo đường ưu tiên.</Text>
                <Text style={styles.h3}> 5. Tại ngã tư các tuyến đường cùng cấp : xe bên phải trống => xe rẽ phải => xe đi thẳng => xe rẽ trái.</Text>
            </ScrollView>
        </SafeAreaView >
    )
}


const styles = StyleSheet.create({
    h1: {
        fontSize: 19,
        fontWeight: 'bold',
        marginVertical: 5
    },
    h2: {
        fontSize: 17,
        fontWeight: 'bold',
        marginLeft: 8
    },
    h3: {
        fontSize: 15,
        marginLeft: 8,
        marginTop: 5
        // fontWeight: 'bold'
    },
    h4: {
        fontSize: 13,
        // fontWeight: 'bold'
    },

    border: {
        borderColor: '#dedede',
        borderWidth: 1,
        // paddingHorizontal: 8,
        paddingVertical: 5
    },
    textCenter: {
        textAlign: 'center',
        marginHorizontal: 7
    }
})

export default Note