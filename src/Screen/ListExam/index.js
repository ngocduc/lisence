import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { ProgressBar, Colors, Card, Chip } from 'react-native-paper'
import Icon from 'react-native-vector-icons/AntDesign';
import { isEmpty } from 'lodash';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

import { getItem, saveItem } from '../../hepper/handleStore';
import { KEY_STORE, NUMBER } from '../../hepper/const';

const { width, height } = Dimensions.get('window');
const listExam = new Array(18).fill(0).map((_, index) => index + 1)
const convertResult = (result) => {
    let correct = 0
    let inCorrect = 0

    if (!isEmpty(result)) {
        Object.keys(result).map(key => {
            if (result[key] === true) {
                correct += 1
            }
            if (result[key] === false) {
                inCorrect += 1
            }
        })
    }
    return {
        correct, inCorrect
    }
}

const ListExam = (props) => {
    const [dataHistory, setDataHistory] = useState({});
    const isFocused = useIsFocused();
    useFocusEffect(
        React.useCallback(() => {
            if (isFocused) {
                _getHis();
            }
            return () => { };
        }, [isFocused])
    );

    const _getHis = async () => {
        const data = await getItem(KEY_STORE.TEST_HIS)
        setDataHistory(data || {})
    }
    return (
        <SafeAreaView>
            <FlatList
                style={{ marginTop: 10 }}
                keyExtractor={(_, index) => 'book_item' + index.toString()}
                numColumns={2}
                data={listExam}
                renderItem={({ item, index }) => {
                    const hisItem = dataHistory[item];
                    const { correct, inCorrect } = convertResult(hisItem);
                    return (
                        <TouchableOpacity
                            onPress={() => { props.navigation.navigate("Test", { test_id: item }) }}
                            style={[styles.item, styles.shadow]}>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#555' }}>Đề thi {item > 9 ? item : `0${item}`}</Text>
                            </View>
                            {hisItem ? <View style={{ flex: 2 }}>
                                <ProgressBar progress={correct / NUMBER.QUESTION} color={'red'} style={{ marginHorizontal: 10, borderRadius: 7, height: 5 }} />
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon style={{ fontSize: 18, color: 'green' }} name="checkcircleo" />
                                        <Text style={{ fontSize: 18, color: 'green', marginLeft: 6 }}>{correct}</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon style={{ fontSize: 18, color: 'red' }} name="closecircleo" />
                                        <Text style={{ fontSize: 18, color: 'red', marginLeft: 6 }}>{inCorrect}</Text>
                                    </View>
                                </View>
                            </View> : null}
                        </TouchableOpacity>
                    )
                }}
            />

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    item: {
        width: width / 2 - 20,
        aspectRatio: 3 / 2,
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    shadow: {
        backgroundColor: '#FFFFFF',
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 10,
        shadowOffset: { width: 12, height: 13 },
    }
})


export default ListExam;
