import * as React from 'react';
import { Animated, View, TouchableOpacity, StyleSheet, Text, Image } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { ScrollView, FlatList } from 'react-native-gesture-handler';

const dnd = new Array(100).fill({
  img: 'https://i.ytimg.com/vi/Hi7zJJrDdnw/maxresdefault.jpg',
  title: 'asdf',
  content: 'https://i.ytimg.com/vi/Hi7zJJrDdnw /maxresdefault.jpghttps://i.ytimg.com/ vi/Hi7zJJrDdnw/maxresdefault.j pghttps://i.ytimg.com/vi/Hi7zJJrDd nw/maxres default.jp ghttps://i.ytimg.com/vi /Hi7zJJrDdnw/maxresdefau lt.jpghttps://i.ytimg.com/vi /Hi7zJJrDdnw/maxresdefau lt.jpghttps://i.ytimg. com/vi/Hi7zJJrDdnw/m axresdefault.jpghtt ps://i.ytimg.com/vi/Hi7zJJrDd nw/maxr esdefault.jpg'
})
const dataFake = [
  {
    img: 'https://i.ytimg.com/vi/Hi7zJJrDdnw/maxresdefault.jpg',
    title: 'asdf',
    content: 'https://i.ytimg.com/vi/Hi7zJJrDdnw /maxresdefault.jpghttps://i.ytimg.com/ vi/Hi7zJJrDdnw/maxresdefault.j pghttps://i.ytimg.com/vi/Hi7zJJrDd nw/maxres default.jp ghttps://i.ytimg.com/vi /Hi7zJJrDdnw/maxresdefau lt.jpghttps://i.ytimg.com/vi /Hi7zJJrDdnw/maxresdefau lt.jpghttps://i.ytimg. com/vi/Hi7zJJrDdnw/m axresdefault.jpghtt ps://i.ytimg.com/vi/Hi7zJJrDd nw/maxr esdefault.jpg'
  }
]
const ItemRender = () => (
  <View style={[styles.container]} >
    <FlatList
      data={dnd}
      keyExtractor={(item, index) => index + ''}
      renderItem={({ item, index }) => {
        return (
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Image style={{ flex: 1, marginRight: 10 }} resizeMode="contain" source={{ uri: item.img }} />

            </View>
            <View style={{ flex: 2 }}>
              <Text style={{ fontSize: 18, fontWeight: '600', marginBottom: 10 }}>{item.title}</Text>
              <Text>{item.content}</Text>
            </View>

          </View>
        )
      }}
    />
  </View>
);

export default class TabViewExample extends React.Component {

  constructor(props) {
    super(props);
    this.flatRef = React.createRef();
  }

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Biển báo cấm' },
      { key: 'second', title: 'Biển hiệu lệnh' },
      { key: 'second1', title: 'Biển chỉ dẫn' },
      { key: 'second2', title: 'Biển nguy hiểm và cảnh báo' },
      { key: 'second3', title: 'Biển phụ' },
    ],
  };

  _handleIndexChange = (index) => {
    this.setState({ index });
    if (this.flatRef.scrollToIndex) {
      this.flatRef.scrollToIndex({ animated: true, index: "" + index });
    }
  };

  _renderTabBar = (props) => {
    return (
      <View style={styles.tabBar}>
        <FlatList
          ref={(ref) => { this.flatRef = ref; }}
          data={props.navigationState.routes}
          horizontal showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index + ''}
          // style={{ paddingHorizontal: 20 }}
          renderItem={({ item: route, index: i }) => {
            return (
              <>
                {i == 0 ? <View style={{ width: 100 }} /> : null}
                <TouchableOpacity
                  style={[styles.tabItem, this.state.index == i ? styles.activeItem : {}]}
                  onPress={() => {
                    this._handleIndexChange(i)
                  }}>
                  <Text style={this.state.index == i ? styles.textActive : styles.textItem}>{route.title}</Text>
                </TouchableOpacity>
              </>
            )
          }}
        />
      </View >
    );
  };

  _renderScene = SceneMap({
    first: () => ItemRender(),
    second: () => ItemRender(),
    second1: () => ItemRender(),
    second2: () => ItemRender(),
    second3: () => ItemRender(),
    second4: () => ItemRender(),
    second5: () => ItemRender(),
  });

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  tabBar: {
    height: 35,
    backgroundColor: '#fff'
  },
  tabItem: {
    flex: 1,
    justifyContent: 'center',
    // paddingHorizontal: 15,
    paddingVertical: 5
    // backgroundColor: 'red'
  },
  activeItem: {
    // backgroundColor: '#efefef',
    borderBottomColor: 'green',
    borderBottomWidth: 2,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  textItem: { marginHorizontal: 25, fontSize: 16, color: '#222' },
  textActive: { marginHorizontal: 25, fontSize: 18, color: '#000', fontWeight: '500' },
});
