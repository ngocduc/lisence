// In App.js in a new project

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/Screen/Home'
import Exam from './src/Screen/Exam'
import Test from './src/Screen/Exam/test'
import Result from './src/Screen/Exam/result'
import ListExam from './src/Screen/ListExam'
import Practice from './src/Screen/Exam/practive'
import Note from './src/Screen/Note'
import TrafficSigns from './src/Screen/TrafficSigns'


const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home" component={Home}
          options={{ title: '600 Câu GPLX Bằng B2' }}
        />
        <Stack.Screen
          name="Exam" component={Exam}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ListExam" component={ListExam}
          options={{ title: 'Danh sách bài thi' }}
        />
        <Stack.Screen
          name="Test" component={Test}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ResultExam" component={Result}
          options={{ title: 'Kết qủa thi' }}
        />
        <Stack.Screen
          name="Practice" component={Practice}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Note" component={Note}
          options={{ title: 'Mẹo ghi nhớ' }}
        />
        <Stack.Screen
          name="TrafficSigns" component={TrafficSigns}
          options={{ title: 'Các biển báo' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;